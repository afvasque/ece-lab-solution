
#include <stdio.h>

#include "user_input.h"

// Function Definition
float receiveUserInput(void)
{
    float usr_in = 0;
    
    printf("Enter value: ");
    scanf("%f", &usr_in);

    return usr_in;
}
