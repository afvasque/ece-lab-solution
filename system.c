
#include <stdio.h>
#include "system.h"

float  data[SYSTEM_DATA_SIZE];
int    current_storage_index;

void systemInit(void)
{
    current_storage_index = 0;
    
    for(int i=0; i<SYSTEM_DATA_SIZE; i++)
    {
        *(data + i) = SYSTEM_VALUE_LIMIT + 1;
    }
}

int systemStoreNewValue(float new_value)
{
    if ( ((SYSTEM_VALUE_LIMIT + 1) > new_value) && (SYSTEM_DATA_SIZE > current_storage_index) )
    {
        *(data + current_storage_index++) = new_value;
        
        return 1;
    }
    
    return 0;
}

int systemSearch(void)
{
    float search_value;

#ifndef PRINTS_OUT_ADDRESSES
    unsigned int   found_item_at = -1;
#else
    unsigned int   found_item_at = NULL;
#endif
    
    printf("\r\nEnter value to search: ");
    scanf("%f", &search_value);
    
    if (0 == current_storage_index)
    {
        printf("No values in storage...");
    }
    else
    {
        for (int i=0; i<current_storage_index; i++)
        {
            if ( *(data + i) == search_value )
            {
#ifndef PRINTS_OUT_ADDRESSES
                found_item_at = i;
#else
                found_item_at = (data + i);
#endif
                i = current_storage_index;
            }
        }
        
#ifndef PRINTS_OUT_ADDRESSES
        if (-1 != found_item_at)   { printf("Found item at: %d", found_item_at); }
#else
        if (NULL != found_item_at) { printf("Found item at: 0x%08x", found_item_at); }
#endif
        else
        {
            printf("Item NOT found...");
        }
    }
    
    return found_item_at;
}

void systemPrintData(void)
{
    int print_index = 0;
    
    printf("\r\n");
    
    for (int i=0; i<SYSTEM_DATA_SIZE; i++)
    {
        for (int j=0; j<SYSTEM_PRINT_COLS; j++)
        {
            if (print_index < current_storage_index)
            {
                
#ifndef PRINTS_OUT_ADDRESSES
                printf("%8.3f "   , *(data + print_index));
#else
                printf("0x%08x   ",  (data + print_index));
#endif
                
                print_index++;
                i++;
            }
            else
            {
                i = SYSTEM_DATA_SIZE;
                j = SYSTEM_PRINT_COLS;
            }
        }
        
        printf("\r\n");
    }
}

void systemRangedCount(void)
{
    float search_value_a;
    float search_value_b;
    
    float temp;
    
    int ocurrence_count = 0;
    
    printf("\r\n\r\nEnter search limit 1: ");
    scanf("%f", &search_value_a);
    
    printf("Enter search limit 2: ");
    scanf("%f", &search_value_b);
    
    // Fixes input order
    if (search_value_a > search_value_b)
    {
        temp = search_value_a;
        search_value_a = search_value_b;
        search_value_b = temp;
    }
    
    for (int i=0; i<current_storage_index; i++)
    {
        if ( (search_value_a <= *(data + i)) && (search_value_b >= *(data + i)) )
        {
            ocurrence_count++;
        }
    }
    
    if (0 < ocurrence_count)
    {
        printf("%d items found!", ocurrence_count);
    }
    else
    {
        printf("Item never found...");
    }
}
