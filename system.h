
#ifndef SYSTEM_H
#define SYSTEM_H

#define SYSTEM_DATA_SIZE     50
#define SYSTEM_VALUE_LIMIT  100
#define SYSTEM_PRINT_COLS     4

// #define PRINTS_OUT_ADDRESSES

// Function Prototype
void systemInit(void);

int  systemStoreNewValue(float new_value);

void systemPrintData(void);

int systemSearch(void);

void systemRangedCount(void);

#endif  /* SYSTEM_H */
