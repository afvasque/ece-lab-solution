/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>

#include "user_input.h"
#include "system.h"

int main()
{
    systemInit();
    
    while (0 < systemStoreNewValue(receiveUserInput()) ) { }
    
    systemPrintData();
    
    systemSearch();
    
    systemRangedCount();

    return 0;
}
